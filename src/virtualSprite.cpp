#include <iostream>
#include "inc/virtualSprite.hpp"

vs::virtualSprite::virtualSprite( ) {
	this->Name = "";
	this->LocX = 0;
	this->LocY = 0;
	this->Width = 0;
	this->Height = 0;
	this->originalSpritesheet = "";
}
vs::virtualSprite::virtualSprite( std::string Name,
		int LocX,
		int LocY,
		int Width,
		int Height,
		std::string originalSpritesheet ) {
	this->Name = Name;
	this->LocX = LocX;
	this->LocY = LocY;
	this->Width = Width;
	this->Height = Height;
	this->originalSpritesheet = originalSpritesheet;
}

void vs::virtualSprite::PrintSprite( ) {
	std::cout << "Name: " << Name << '\n'
		<< "LocX: " << LocX << '\n'
		<< "LocY: " << LocY << '\n'
		<< "Width: " << Width << '\n'
		<< "Height: " << Height << '\n'
		<< "originalSpritesheet: " << originalSpritesheet << "\n\n";
}

#include <string>
#include <fstream> 
#include <iostream>
#include <sstream> 
#include "inc/spriteMap.hpp"
#include "inc/virtualSprite.hpp"

void sm::spriteMap::AddVirtualSprite( ){
	sprite.push_back( vs::virtualSprite( ) );
}

void sm::spriteMap::AddVirtualSprite( std::string Name,
		int LocX,
		int LocY,
		int Width,
		int Height,
		std::string originalSpritesheet ) {
	sprite.push_back(vs::virtualSprite( Name, LocX, LocY, Width, Height, originalSpritesheet ) );
}

void sm::spriteMap::AddVirtualSprite(vs::virtualSprite SpriteToAdd)
{
	sprite.push_back(SpriteToAdd);
}

void sm::spriteMap::SortByWidth( ) {
	// Reverse insertion sort (greatest to smallest)

	std::cout << "Sorting by Width...\n";
	int j;

	for ( unsigned long int i = 1; i < sprite.size(); ++i ) {
		vs::virtualSprite tempSprite = sprite[i];
		j = i - 1;
		
		while ( ( j >= 0 ) && ( sprite[j].Width < tempSprite.Width ) ) {
			sprite[j + 1] = sprite[j];
			j = j - 1;
		}
		sprite[j + 1] = tempSprite;
	}
}

void sm::spriteMap::SortByHeight( ) {
	// Reverse insertion sort (greatest to smallest)

	std::cout << "Sorting by Height...\n";
	int j;

	for ( unsigned long int i = 1; i < sprite.size(); ++i ) {
		vs::virtualSprite tempSprite = sprite[i];
		j = i - 1;
		
		while ( ( j >= 0 ) && ( sprite[j].Height < tempSprite.Height ) ) {
			sprite[j + 1] = sprite[j];
			j = j - 1;
		}
		sprite[j + 1] = tempSprite;
	}
}

void sm::spriteMap::SpriteMapInit( std::string spriteMap ) {
	std::stringstream spriteMapStream( spriteMap, std::ios::in );

	std::string Name;
	std::string LocXTemp, LocYTemp; //Will convert to int with sstream
	std::string WidthTemp, HeightTemp;
	int LocX, LocY;
	int Width, Height;
	std::string originalSpritesheet;

	if( spriteMapStream ) {
		std::string spriteMapLine = "";
		while( std::getline( spriteMapStream, spriteMapLine ) ) {

			std::stringstream spriteMapLineSS( spriteMapLine );

			getline( spriteMapLineSS, Name, ',' );
			getline( spriteMapLineSS, LocXTemp, ',' );
			getline( spriteMapLineSS, LocYTemp, ',' );
			getline( spriteMapLineSS, WidthTemp, ',' );
			getline( spriteMapLineSS, HeightTemp, ',' );
			getline( spriteMapLineSS, originalSpritesheet, ',' );

			std::stringstream LocXSS( LocXTemp );
			std::stringstream LocYSS( LocYTemp );
			std::stringstream WidthSS( WidthTemp );
			std::stringstream HeightSS( HeightTemp );
			
			LocXSS >> LocX;
			LocYSS >> LocY;
			WidthSS >> Width;
			HeightSS >> Height;

			AddVirtualSprite( Name, LocX, LocY, Width, Height, originalSpritesheet );
		}
	}
}

sm::spriteMap::spriteMap( ) {
	std::string spriteMap;
	SpriteMapInit( spriteMap );
}

sm::spriteMap::spriteMap( char spriteMapPath[] ) {
	std::ifstream spriteMapStream( spriteMapPath, std::ios::in );
	std::string spriteMap = "";
	std::string spriteMapLine = "";

	while( getline( spriteMapStream, spriteMapLine ) ) {
		if( spriteMapLine != "" ) {
			spriteMap = spriteMap + spriteMapLine + "\n";
		}
	}
	SpriteMapInit( spriteMap );
}

sm::spriteMap::spriteMap( std::string spriteMap ) {
	SpriteMapInit( spriteMap );
}

void sm::spriteMap::PrintSpriteMap( ) {
	for( uint i = 0; i < sprite.size(); ++i ) {
		sprite[i].PrintSprite();
	}
}

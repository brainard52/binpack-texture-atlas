#ifndef SPRITEMAP_HPP
#define SPRITEMAP_HPP
#include <string>
#include <vector>
#include "virtualSprite.hpp"

namespace sm {
	
	class spriteMap {
		public:
			std::vector<vs::virtualSprite> sprite;
			
			void AddVirtualSprite( );
			void AddVirtualSprite( std::string Name, 
					int LocX, 
					int LocY,
					int Width,
					int Height,
					std::string originalSpritesheet );
			void AddVirtualSprite(vs::virtualSprite SpriteToAdd);

			spriteMap( );
			spriteMap( char spriteMapPath[] );
			spriteMap( std::string spriteMap );
			void PrintSpriteMap();

			void SortByWidth();
			void SortByHeight();

		private:
			void SpriteMapInit( std::string spriteMap );

	};
}

#endif

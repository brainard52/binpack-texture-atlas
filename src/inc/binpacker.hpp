#ifndef BINPACKER_HPP
#define BINPACKER_HPP

#include <SFML/Graphics.hpp>
#include <vector>

#include "virtualSprite.hpp"
#include "spriteMap.hpp"

class virtualTextureRow : public std::vector<vs::virtualSprite> {
	public:
		int Width = 0;
		int Height = 0;
		int LocY = 0;
//		virtualTextureRow();
};

class virtualTexture : public std::vector<virtualTextureRow> {
	public:

		// For whatever reason, Width is 5 on instantiation if I don't
		// define these.
		int Width = 0;
		int Height = 0;

//		virtualTexture();
};

class binpacker {
	public:
		binpacker();

		void AddVirtualSprite(vs::virtualSprite);
		//sf::Texture* GenerateTexture();
		void FakeGenerateTexture();


		sm::spriteMap OldSprites;
	private:
		virtualTexture VirtualTexture;		
		//std::vector<std::vector<vs::virtualSprite>> VirtualTexture;
		void AddVirtualTextureRow(vs::virtualSprite);

};

#endif

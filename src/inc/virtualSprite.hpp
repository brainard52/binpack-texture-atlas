#ifndef VIRTUALSPRITE_HPP
#define VIRTUALSPRITE_HPP

#include <string>

namespace vs {
	class virtualSprite {
		public:
			std::string Name; // Name of sprite, arbitrary..
			int LocX, LocY; // top left corner, 0-aligned
			int Width, Height;
			std::string originalSpritesheet;

			virtualSprite( );
			virtualSprite ( std::string Name, 
					int LocX, 
					int LocY,
					int Width,
					int Height,
					std::string originalSpritesheet );

			void PrintSprite();
	};
}
#endif

#include <iostream>
#include "inc/spriteMap.hpp"
#include "inc/binpacker.hpp"

int main()
{
	char spriteMapPath[] = "assets/spritemap";
	sm::spriteMap mySpriteMap( spriteMapPath );
	binpacker myBinpacker;

	for(int i = 0; i < mySpriteMap.sprite.size(); ++i) {
		myBinpacker.AddVirtualSprite(mySpriteMap.sprite[i]);
	}

	myBinpacker.FakeGenerateTexture();
	std::cout << "________________\n";
	myBinpacker.OldSprites.PrintSpriteMap();
	std::cout << "Number of Old Sprites: " << myBinpacker.OldSprites.sprite.size() << '\n';
	return 0;
}

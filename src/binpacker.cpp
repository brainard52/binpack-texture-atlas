#ifdef DEBUG
#undef DEBUG
#endif
#define DEBUG

#include <iostream>
#include <SFML/Graphics.hpp>
#include <vector>

#include "inc/virtualSprite.hpp"
#include "inc/binpacker.hpp"
binpacker::binpacker()
{
}

void binpacker::AddVirtualSprite(vs::virtualSprite SpriteToAdd)
{
	bool added = false;
	int row = 0;
	#ifdef DEBUG
	std::cout << "\nSprite \"" << SpriteToAdd.Name << "\"\n";
	std::cout << "SpriteToAdd Width: " << SpriteToAdd.Width << "\nSpriteToAdd Height: " << SpriteToAdd.Height << '\n';
	std::cout << "Texture Width: " << VirtualTexture.Width << "\nTexture Height: " << VirtualTexture.Height << '\n';
	#endif
	do {
		if(row > static_cast<int>(VirtualTexture.size())-1) {
			/* If there are no other rows to check. I put this
			 * first so it creates a new row for the first sprite 
			 * added. 
			 */
			#ifdef DEBUG
			std::cout << "No more rows to check. Creating new row and adding sprite to that row. It will be row " << row << '\n';
			#endif
			binpacker::AddVirtualTextureRow(SpriteToAdd);
			added = true;
		} else if(VirtualTexture[row][0].Height < SpriteToAdd.Height){
			/* Checks to see if the sprite will fit in the current
			 * row height-wise. If not, it checks the next row. 
			 */
			#ifdef DEBUG
			std::cout << SpriteToAdd.Name << " is too tall for row " << row << ". Checking next row.\n";
			#endif
			++row;
		} else if(VirtualTexture[row].Width >= VirtualTexture.Width) {
			/* This is kinda meant to make it so it fills more
			 * rows to conserve pixels. If there were 100 10x10
			 * sprites, and the first row were 10 tall, the first
			 * row would be 1000+ pixels wide. If that happened,
			 * there is likely going to be a ton of excess space
			 * in the rest of the sprite map which is what we're
			 * trying to avoid here.
			 */
			#ifdef DEBUG
			std::cout << "Width of row " << row << ": " << VirtualTexture[row].Width << "\nHeight of row " << row << ": " << VirtualTexture[row].Height << '\n';
			std::cout << row << " is the widest row with a width of " << VirtualTexture[row].Width << ". Checking next row.\n";
			#endif
			++row;
		} else {
			#ifdef DEBUG
			std::cout << "Sprite was added to row " << row << '\n';
			#endif
			OldSprites.sprite.push_back(SpriteToAdd);
			/* Note that OldSprites is updated BEFORE we modify
			 * SpriteToAdd at all.
			 */

			SpriteToAdd.LocY = VirtualTexture[row].LocY;
			SpriteToAdd.LocX = VirtualTexture[row].Width;

			VirtualTexture[row].push_back(SpriteToAdd);

			VirtualTexture[row].Width+=SpriteToAdd.Width;
			if(VirtualTexture.Width < VirtualTexture[row].Width) {
				VirtualTexture.Width = VirtualTexture[row].Width;
			}
			added = true;
		}

	}while(!added);
	#ifdef DEBUG
	for(int i = 0; i < VirtualTexture.size(); ++i){
		std::cout << "Row: " << i << '\n';
		std::cout << "Width: " << VirtualTexture[i].Width << '\n';
		std::cout << "Height: " << VirtualTexture[i].Height << '\n';
	}
	std::cout << "Texture Width: " << VirtualTexture.Width << '\n';
	std::cout << "Texture Height: " << VirtualTexture.Height << '\n';
	#endif
}

// sf::Texture* binpacker::GenerateTexture();
void binpacker::FakeGenerateTexture() {
	for(int y = 0; y < VirtualTexture.size(); ++y){
		std::cout << "Row: " << y << '\n';
		for(int x = 0; x < VirtualTexture[y].size(); ++x){
			VirtualTexture[y][x].PrintSprite();
		}
	}
}
void binpacker::AddVirtualTextureRow(vs::virtualSprite SpriteToAdd)
{
	VirtualTexture.push_back(virtualTextureRow());
	VirtualTexture[VirtualTexture.size()-1].push_back(SpriteToAdd);

	if(VirtualTexture.size() > 1) {
		VirtualTexture[VirtualTexture.size()-1].LocY = VirtualTexture[VirtualTexture.size()-2].Height; // Remember that LocY is 0-aligned while height and width are not. 
	} else {
		VirtualTexture[VirtualTexture.size()-1].LocY = 0;
		VirtualTexture.Width = SpriteToAdd.Width;
	}

	VirtualTexture[VirtualTexture.size()-1].Height = SpriteToAdd.Height;
	VirtualTexture[VirtualTexture.size()-1].Width = SpriteToAdd.Width;


	if(SpriteToAdd.Width > VirtualTexture.Width){
		VirtualTexture.Width = SpriteToAdd.Width;
	}
	VirtualTexture.Height += SpriteToAdd.Height;
	OldSprites.sprite.push_back(SpriteToAdd);
}

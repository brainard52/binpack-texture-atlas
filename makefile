OBJS = src/main.cpp src/spriteMap.cpp src/virtualSprite.cpp src/binpacker.cpp
CC = clang++
#CC = g++
COMPILER_FLAGS_LENIENT = -std=c++11
COMPILER_FLAGS = -Wall -Wextra -std=c++11 -pedantic
COMPILER_FLAGS_DEBUG = -Wall -Wextra -std=c++11 -pedantic -g
LINKER_FLAGS = -lsfml-graphics -lsfml-window -lsfml-system
OBJ_NAME = bin/game.bin

all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)

lenient : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS_LENIENT) $(LINKER_FLAGS) -o $(OBJ_NAME)

debug : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS_DEBUG) $(LINKER_FLAGS) -o $(OBJ_NAME)


